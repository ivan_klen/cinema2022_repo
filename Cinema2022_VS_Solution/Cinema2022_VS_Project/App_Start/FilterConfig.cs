﻿using System.Web;
using System.Web.Mvc;

namespace Cinema2022_VS_Project
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
